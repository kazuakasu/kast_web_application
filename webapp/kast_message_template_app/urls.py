from django.conf.urls import url
from . import views

urlpatterns = [
    # Route to message template landing page
    url(r'^message-template/$', views.message_template, name='message-template'),
]
