from django.conf.urls import url
from . import views

urlpatterns = [
    # Route to sender id template landing page
    url(r'^users/$', views.home, name='home'),
    url(r'^users/form/$', views.form, name='form'),
]
