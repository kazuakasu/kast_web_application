from django.db import models
from django_extensions.db.fields import ShortUUIDField

from kast_account_app.models import KastTenants


class KastSidsManager(models.Manager):
    def get_sender_id(self, sender_id, tenant_id):

        """

        Checks if sender ID of a tenant exists.
        Returns sender ID object

        :type sender_id: int
        :param sender_id: The sender ID

        :type tenant_id: int
        :param tenant_id: The tenant ID

        :rtype: obj
        :return: Returns sender ID info if sender ID exists within a given tenant ID    

        """
        
        sender_ids = self.filter(name=sender_id, tenant_id=tenant_id)
        for _ in sender_ids:
            if _.name == sender_id:
                return _
        return None


class KastSIDs(models.Model):
    PENDING = 0
    APPROVED = 1
    REJECTED = 2
    DELETED = 3
    CHOICES = (
        (APPROVED, 'Approved'),
        (PENDING, 'Pending'),
        (REJECTED, 'Rejected'),
        (DELETED, 'Deleted')
    )

    uid             = ShortUUIDField(unique=True, null=True)
    tenant          = models.ForeignKey(KastTenants, on_delete=models.CASCADE)
    name            = models.CharField(max_length=30)
    requested_by    = models.CharField(max_length=100)
    status          = models.IntegerField(choices=CHOICES,default=PENDING)
    created         = models.DateTimeField(auto_now_add=True)
    modified        = models.DateTimeField(auto_now=True)
    objects         = KastSidsManager()

    class Meta:
        db_table = "kast_sids"

    def __unicode__(self):
        return self.name or u''