# Django settings for webapp project.
import os
import ast  # py 2.6 only

def env(key, default=None, valuetype=str, required=False, nullable=bool):
    if required and (key not in os.environ):
        raise RuntimeError(u"Required environment setting %s not found" % key)
    if valuetype == bool:
        # special handling of booleans: must be a valid python expr: True or False
        raw_val = default
        if key in os.environ:
            raw_val = ast.literal_eval(os.environ.get(key))
    else:
        raw_val = os.environ.get(key, default)
    val = valuetype(raw_val)
    if nullable and (default is None) and (raw_val == default):
        val = default
    return val

BASE_DIR           = os.path.dirname(os.path.dirname(__file__))
SECRET_KEY         = env('WEBAPP_SECRET_KEY', required=True)
DEBUG              = env('WEBAPP_DEBUG', valuetype=bool, required=True)
TEMPLATE_DEBUG     = env('WEBAPP_TEMPLATE_DEBUG', valuetype=bool, required=True)
MAIN_DOMAIN        = env('WEBAPP_MAIN_DOMAIN', valuetype=unicode, required=True)
LOGIN_URL          = '/home'

# Application definition
INSTALLED_APPS = (

    # Framework tools
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'channels',

    # Developed Applications
    'kast_auth_app',
    'kast_home_app',
    'kast_account_app',
    'kast_campaign_app',
    'kast_message_template_app',
    'kast_bcast_app',
    'kast_sender_id_app',
    'kast_contact_app',
    'kast_user_management_app'
)

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'asgi_redis.RedisChannelLayer',
        'CONFIG': {
            'hosts': [('localhost', 6379)],
        },
        'ROUTING': 'webapp.routing.channel_routing',
    }
}

MIDDLEWARE_CLASSES = (
    'webapp.middleware.KastApiExceptionMiddleware',
    'django.middleware.gzip.GZipMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF     = 'webapp.urls'
WSGI_APPLICATION = 'webapp.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE':   'django.db.backends.mysql',
        'NAME':     env('WEBAPP_DB_NAME', required=True),
        'USER':     env('WEBAPP_DB_USER', required=True),
        'PASSWORD': env('WEBAPP_DB_PASS', required=True),
        'HOST':     env('WEBAPP_DB_HOST', 'localhost'),
        'PORT':     env('WEBAPP_DB_PORT', '3306'),
        'OPTIONS': {
            'sql_mode': 'TRADITIONAL',
            'charset': 'utf8',
            'init_command': 'SET character_set_connection=utf8, \
                             collation_connection=utf8_bin'
        },
    }
}

LANGUAGE_CODE = 'en-us'
TIME_ZONE     = env('WEBAPP_TIME_ZONE', valuetype=str, required=True)
USE_I18N      = True
USE_L10N      = True
USE_TZ        = True

# Media Files
ADMIN_MEDIA_PREFIX = '/static/admin/'
MEDIA_ROOT         = env('WEBAPP_MEDIA_ROOT', valuetype=str, required=True)
MEDIA_URL_DOMAIN   = env('WEBAPP_MEDIA_URL_DOMAIN', '', valuetype=str, required=True)
MEDIA_URL_PATH     = env('WEBAPP_MEDIA_URL_PATH', valuetype=str, required=True)
MEDIA_URL          = '//' + MEDIA_URL_DOMAIN + MEDIA_URL_PATH

# Static Files (CSS, JavaScript, Images)
STATIC_ROOT       = env('WEBAPP_STATIC_ROOT', valuetype=str, required=True)
STATIC_URL_DOMAIN = env('WEBAPP_STATIC_URL_DOMAIN', '', valuetype=str, required=True)
STATIC_URL_PATH   = env('WEBAPP_STATIC_URL_PATH', valuetype=str, required=True)
STATIC_URL        = STATIC_URL_PATH
#'//' + STATIC_URL_DOMAIN + STATIC_URL_PATH

# Additional locations of static files
STATICFILES_DIRS = (
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__), '..', '_static'),
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.request",
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    'django.core.context_processors.request'
)

TEMPLATE_DIRS = (
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(os.path.dirname(__file__), '..', '_templates'),
)

# Authentication related signaling
AUTH_PROFILE_MODULE = 'kast_account_app.KastUsers'

# Custom auth backend
AUTHENTICATION_BACKENDS = [
    'kast_auth_app.auth.AuthBackend',
    'kast_auth_app.auth.AuthBackendNoPassword',
]

# Email Credentials for SMTP
EMAIL_BACKEND       = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_USE_TLS       = True
EMAIL_HOST          = 'smtp.gmail.com'
EMAIL_HOST_USER     = 'customercare@kast.ph' #'kast_admin@risingtide.ph'
EMAIL_HOST_PASSWORD = 'cozwecar3!' #'rt-kast2017!'
EMAIL_PORT          = 587
DEFAULT_FROM_EMAIL  = 'customercare@kast.ph' #'kast_admin@risingtide.ph'

# Allowed Hosts
ALLOWED_HOSTS = [
    MAIN_DOMAIN,
    MEDIA_URL_DOMAIN,
    STATIC_URL_DOMAIN,
    'www.kast.ph',
    'kast.ph',
]

AWS = {
    'access_key_id': 'AKIAI4BAFOLNL5JLFACQ',
    'secret_access_key': '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',
    's3': {
        'bucket': 'rtbenjamin',
        'url_expires_in': 86400, # 24h
    },
}

BROADCASTS = {
    #'base_url': 'http://172.30.0.110/broadcasts/',
    'base_url': 'http://52.221.130.226/broadcasts/',
    'report_url': 'http://52.76.39.9/receipts/',
    'status_url': "http://52.76.39.9/report/{bcast}/?status={status}",
    'user': 'bcast',
    'pass': 'kastengine',
    'sender_id': 'KAST',
}

# This setting is only for catching uncaught exceptions.
# Always use Django's logger 'django.server' for logging.
# E.g. `logger = logging.getLogger('django.server')`.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'root': {
        'level': 'DEBUG',
        'handlers': ['file'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                '%(process)d %(thread)d %(message)s'
        }
    },
    'loggers': {
        'django': {
            'propagate': True,
        },
    },
    'handlers': {
        'file': {
            'level': 'INFO',
            'formatter': 'verbose',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '../log/kast.log',
            'maxBytes': 1024*1024*10, # 10MB
            'backupCount': 5,
        }
    }
}

