// Initialize Example 1
var table = $('#dashboard-table').DataTable( {
    "scrollX": false,
    "pageLength": 10,
    "bInfo" : true,
    "order": [[ 0, "desc" ]],
    dom: "<'row'<'col-sm-12 custom-filters'f>>t<'row'<'col-sm-12 bottom-filters'i>>p",
    "pagingType": "full_numbers",
    "language": {
        "lengthMenu": "Rows:<br> _MENU_",
        "infoEmpty": "No records available",
        "search": "Search: ",
        "paginate": {
            "previous": "Previous",
            "next": "Next",
            "first": "First",
            "last": "Last"
        }
    },
    scrollCollapse: false,
    "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            {
                "targets": [4],
                "visible": true,
                "orderable": false
            }
        ]
});

var filter_field = '<div class="filter-container form-inline" style="width:30%;"><div class="form-group" style="width:100%;"><label for="filter" class="filter-label">Permissions&nbsp;&nbsp; </label><select class="filter form-control" style="width: 70%;"></select></div></div>';

$('#dashboard-table_filter').addClass('form-inline');
$('#dashboard-table_filter input').css('margin-left', '5px');
$('#dashboard-table_filter input').addClass('form-control');

$(".custom-filters #dashboard-table_filter").before($(filter_field));

/*delete user modal confirmation*/
$(".dataTables_wrapper").on('click', '.delete-user', function(){
    $("#myModalLabel").html("Delete User");
    $(".action-delete").show();
    $(".successful-delete").hide();
    $(".action-btn").show();
    $(".success-btn").hide();
    $("#deleteUser").modal("show");
});

/*process delete user*/
$(".dataTables_wrapper").on('click', '.process-delete-user', function(){
    $("#myModalLabel").html("User Deleted");
    $(".action-delete").hide();
    $(".successful-delete").show();
    $(".action-btn").hide();
    $(".success-btn").show();
});

var form = $("#user-form");

$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
});

$.validator.addMethod("validemail", function(value, element) {
    return /^[\w_\-\.]+@[\w_-]+\.[a-z|A-Z]+$/i.test(value);
}, "Please input a valid email address");

form.validate({
    errorPlacement: function errorPlacement(error, element) { 
        //element.before(error); 

        if(element.attr("name") == "firstname"){
            error.insertAfter("input[name=firstname]");
        }

        if(element.attr("name") == "lastname"){
            error.insertAfter("input[name=lastname]");
        }

        if(element.attr("name") == "email"){
            error.insertAfter("input[name=email]");
        }
    },
    rules: {
        email: {
            required: true,
            validemail: true,
            maxlength: 100,
        },
        /*firstname: {
            NoWhiteSpaceAtBeginn: true
        },
        lastname: {
            NoWhiteSpaceAtBeginn: true
        }*/
    },
    /*messages: {
        email: {
            NoWhiteSpaceAtBeginn: "Email must not begin with a whitespace"
        }
    },*/
    submitHandler: function(form) {

        $("#addedUser").modal('show');

        return false;
    }
});

$('#addedUser').on('hidden.bs.modal', function (e) {

    if(!$('#addedUser').find('.btn-user-permissions').hasClass('from-added-btn')){

        window.location = '/users/';
    }

});

$(".btn-user-permissions").click(function(){

    $(".user-form-container .nav-tabs li").removeClass('active');
    $(".user-form-container .tab-content .tab-pane").removeClass('active');

    //activate permissions tab
    $(".user-form-container .nav-tabs li:last-child").addClass('active');
    $(".user-form-container .tab-content #tab-2").addClass('active');

    //add class for checking redirection of modal close
    $(this).addClass('from-added-btn');

    //close added user modal
    $("#addedUser").modal('hide');
});