var form = $("#broadcast-container");
var contactIds = [];
var checkedValues = [];
var maxBroadcastLength = 459;
var csrf = csrftoken;
var originalText = $("#loading").text(),i  = 0, stopAnimate = false;

var strdisbaledTime = getTime();
var minDate = 0;

var currentdate = new Date();
var hours = currentdate.getHours();
var minutes = currentdate.getMinutes();

//timestamp
var current_time = ((hours * 3600000) + (minutes * 60000));
var fromTwelveMidnight = ((00 * 3600000) + (00 * 60000));
var toSevenFiftyNineAm = ((07 * 3600000) + (59 * 60000));
var fromEightAm = ((08 * 3600000) + (00 * 60000));
var toSixPm = ((18 * 3600000) + (00 * 60000));
var fromSixOnePm = ((18 * 3600000) + (01 * 60000));
var toElevenFiftyNinePm = ((23 * 3600000) + (59 * 60000));
var isPaused = false;

//set 1 second interval to disable schedule immediately if the time is before 8am and after 6pm
setInterval(function(){

    if(!isPaused){

        getSendNowStat();
    }

}, 1000);


/**
* check current timestamp to disable send now button
* 
*/
function getSendNowStat(){

    /*between 8am to 6pm - enabled send now button*/
    if(current_time >= fromEightAm && current_time <= toSixPm){

        $("input[name=check-send-now]").removeAttr('disabled');
        $(".sched-now").css('color', '#fff');

    }

    /*between 6:01pm to 11:59pm - disabled send now button*/
    if(current_time >= fromSixOnePm && current_time <= toElevenFiftyNinePm){

        $("input[name=check-send-now]").attr('disabled', 'disabled');
        $(".sched-now").css('color', '#d9d9d9');
    }

    /*between 12 midnight to 7:59am - disabled send now button*/
    if(current_time >= fromTwelveMidnight && current_time <= toSevenFiftyNineAm){

        $("input[name=check-send-now]").attr('disabled', 'disabled');
        $(".sched-now").css('color', '#d9d9d9');
    }
}

/**
* Validation rule for utf characters only
* 
*/
$.validator.addMethod("utfonly", function(value, element) {

    return unicodeTest.test(value);

}, "We noticed you used unsupported characters"); 


/**
* Validation rule for checking white spaces at the beginning of input/select element
* 
*/
$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
  });

/**
* Broadcast form wizard validation
* 
*/
form.validate({
    errorPlacement: function errorPlacement(error, element) { 
        //element.before(error); 
        if(element.attr("name") == "contact_group"){
            error.insertAfter(".contacts-container");
        }
        if(element.attr("name") == "campaign_name"){
            error.insertAfter("input[name=campaign_name]");
        }
        if(element.attr("name") == "broadcast_message"){
            error.insertAfter(".textarea-wrapper");
        }
        if(element.attr("name") == "schedule_date"){
            error.insertAfter(".datepicker-sched");
        }
        if(element.attr("name") == "schedule_time"){
            error.insertAfter(".timepicker-sched");
        }
        if(element.attr("name") == "select_sender_id"){
            error.insertAfter(".sender_id_error");
        }
    },
    rules: {
        contact_group: {
            required: true
        },
        broadcast_message: {
            required: true,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true
        },
        schedule_date: {
            required: true
        },
        schedule_time: {
            required: true
        },
        campaign_name: {
            required: true,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true,
            maxlength: 50
        },
        select_sender_id: {
            required: true
        }
    },
    messages: {
        contact_group: "You must select at least one contact group",
        broadcast_message: {
            required: "Broadcast message is required",
            NoWhiteSpaceAtBeginn: "Broadcast message must not begin with a whitespace"
        },
        campaign_name: {
            required: 'Campaign name is required',
            NoWhiteSpaceAtBeginn: "Campaign name must not begin with a whitespace",
            maxlength: 'Please enter no more than 50 characters'
        },
        select_sender_id: {
            required: "Sender ID is required"
        }
    }
});

/**
* Convert datetime to timestamp
* 
*/
function getEpochTime(){

    var datetime = $("#datepicker").val() + " " + $("#timepicker").val();
    var ddate = new Date(datetime);
    var epoch_time = ddate.getTime() / 1000;

    return epoch_time;
}

function getDisabledTime(){

    /**
    * Between 8am to 6pm
    * 
    */
    if(current_time >= fromEightAm && current_time <= toSixPm){

        //convert 12 hour to military hour
        explode_hours = interval_hours.split(":");
        vhours = ((explode_hours[0] * 3600000) + (explode_hours[1] * 60000));
        
        /**
        * if hour is beyond 6:01pm - schedule date starts from next day
        * if between 8m to 6:00pm - schedule date starts from current day
        */
        if(vhours > 64800000){

            minDate = 1;
            strdisbaledTime = "7am";
        }
        else{

            minDate = 0;
            strdisbaledTime = getTime();
        }
    }

    /**
    * Between 6:01pm to 11:59pm
    * schedule date start from next day
    * schedule time starts from 7am
    */
    if(current_time >= fromSixOnePm && current_time <= toElevenFiftyNinePm){

        minDate = 1;
        strdisbaledTime = "7am";
    }

    /**
    * Between 12midnight to 7:59am
    * schedule date starts from current day
    * schedule time starts from 7am
    */
    if(current_time >= fromTwelveMidnight && current_time <= toSevenFiftyNineAm){

        minDate = 0;
        strdisbaledTime = "7am";
    }
}


/**
* Integration of Angular 1 directive script for jquery steps wizard
* 
*/
app.directive('wizard', [function() {
    return {
        restrict: 'EA',
        scope: {
            stepChanging: '='
        },
        compile: function(element, attr) {
            element.steps({
                bodyTag: attr.bodyTag,
                transitionEffect: "slideLeft",
                autoFocus: true,
                titleTemplate: '<div style="width:100%;"><span class="steps-index">#index#</span></div><span class="steps-text">#title#</span>',
                labels: {
                    next: "NEXT",
                    previous: "PREVIOUS",
                    finish: 'NEXT',
                    loading: "Loading ..."
                },
                autoFocus: true,
                enableContentCache: false,
                preloadContent: true,
                saveState: true,
                startIndex: 0,
                onInit: function (event, currentIndex) {

                    $(".broadcast-container .content").css('min-height', '26em');
                        
                    /**
                    * Action broadcast wizard form after closing summary broadcast modal
                    * 
                    */
                    $('#broadcast-summary').on('hidden.bs.modal', function (e) {

                        $(".error-broadcast").css('display', 'none');
                        $(".error-broadcast").find(".error-text").html("");
                      
                        if($(".btn-submit-broadcast").data('error') == "invalid_data"){

                            window.location = 'broadcasts/live';
                        }
                        if($(".btn-submit-broadcast").data('error') == "invalid_schedule" || $(".btn-submit-broadcast").data('error') == "invalid_campaign"){

                            element.steps('previous');
                        }

                        if($(".btn-submit-broadcast").data('error') == "invalid_schedule_time"){
                            $("#broadcast-summary").modal("hide");
                            $(".modal-backdrop").remove();
                        }

                    });

                    /**
                    * Get sender id from wizard form sender ids dropdown
                    * 
                    */
                    var text_sender_id = $("#select-sender-id").find("option:first-child").val();

                    /*Get sender id if campaign status is draft*/
                    if($(".btn-save-campaign-draft button").data('draft')){

                        text_sender_id = $("#select-sender-id").find("option:selected").text();
                    }

                    /*container for sender id in phone simulation*/
                    $(".message-sender-id").html(text_sender_id);

                    /*container for sender id in summary modal*/
                    $(".sender-id").html(text_sender_id);
                    
                    $("#select-sender-id").change(function(){

                        var val_text = $(this).find("option:selected").text();

                        $(".message-sender-id").html(val_text);
                        $(".sender-id").html(val_text);

                    });

                    /**
                    * Get data if message template has initial message, if not, get message from message template dropdown
                    * 
                    */
                    var message_draft = $("#message_template").find('option:nth-child(2)').data('initial');
                    if(message_draft){
                        message_temp = message_draft;
                    }
                    else{
                        message_temp = $("#message_template").find('option:nth-child(2)').data('message');
                    }

                    /*set message box counter based on message length*/
                    getCounter(message_temp.length);

                    /*container for message in phone simulator*/
                    $(".sms-text p").find('span').html(message_temp.replace(/</g, "&lt;").replace(/>/g, "&gt;")).text();
                    
                    /*container for message in broadcast summary modal*/
                    $(".message-body").html(message_temp.replace(/</g, "&lt;").replace(/>/g, "&gt;")).text();
                    
                    /*textarea for message body*/
                    $("#broadcast_message").val(message_temp);
                    
                    /*container for message character counter*/
                    $('#characterLeft').text(maxBroadcastLength - message_temp.length);
                    
                    $("#message_template").change(function(){

                        var data_message = $(this).find("option:selected").data('message');

                        if($(this).val() == "blank"){

                            $(".sms-text p").find('span').html("");
                            $("#charCounter").text("0");
                        }

                        var ch = data_message.length - 2;
                        $('#characterLeft').text(maxBroadcastLength - ch);
                        getCounter(ch);

                        $("#form_broadcast_message").val(data_message);
                        $(".sms-text p").find('span').html(data_message).text();
                        $(".message-body").html(data_message).text();
                        $("#broadcast_message").val(data_message);

                    });

                    /*hide next button in form wizard in first step*/
                    $('.actions > ul > li:first-child').attr('style', 'display:none');

                    $('.wizard>.actions>ul').css('margin-left', '25px');

                    //initially disable wizard step 1 next button
                    disableWizardNext();

                    $('#broadcast_message').keyup(function () {

                        var len = $(this).val().length;

                        getCounter(len);

                        //message template point to create message option
                        $("#message_template").val("blank");

                        var ch = maxBroadcastLength - len;
                        $('#characterLeft').text(ch);

                        var post = $(this).val().replace(/</g, "&lt;").replace(/>/g, "&gt;");

                        $(".sms-text p").find('span').html(post).text();
                        $(".message-body").html(post).text();
                        
                    });

                    /**
                    * Save draft campaign action
                    * @param {string} campaign_name
                    * @param {string} schedule - timestamp value for schedule date and time
                    * @param {integer} sender_id
                    * @param {string} message
                    * @param {integer} campaign_id
                    * @param {array} contact_grous - lists of selected contact groups in first step of form wizard
                    */

                    $(".btn-save-campaign-draft button").click(function(){

                        if($("#form_schedule").val() == 'None'){

                            $("#form_schedule").val('');
                        }

                        var data = {
                            campaign_name:  $("#campaign_name").val(),
                            schedule:  $("#form_schedule").val() || null, 
                            sender_id    :  $("#select-sender-id").val(),
                            message      :  $("#broadcast_message").val(),
                            campaign_id  :  $("#campaign_id").val(),
                            contact_groups: contactIds
                        }
                        
                        $.LoadingOverlay("show", {
                            color: "rgba(255, 255, 255, 0.42)"
                        });
                        
                        $.ajax({
                            url: '/broadcasts/save_draft/',
                            method: "POST",
                            dataType: 'json',
                            headers: {'X-CsrfToken': csrf},
                            data: JSON.stringify(data), 
                        }).done(function(msg) {
                            
                            $.LoadingOverlay("hide");    
                            $("#save-success").modal('show');

                            $("#campaign_id").val(msg.campaign_id);

                        }).fail(function(jqXHR, textStatus){

                            $.LoadingOverlay("hide");

                            try{

                                var error = jqXHR.responseText;
                                var data_error = $.parseJSON(error);
                                
                                /*campaign name already exists error*/
                                if(data_error.code == '012'){

                                    $("#contact-group-warning").find(".savedraft-error").html(data_error.error);

                                    $("#contact-group-warning").modal({backdrop: 'static',
                                                    keyboard: false});
                                }
                            }
                            catch(error){

                                $("#contact-group-warning").find(".savedraft-error").html("Error in saving draft. Please contact Rising Tide");

                                $("#contact-group-warning").modal({backdrop: 'static',
                                                keyboard: false});
                            }

                        });
                    });

                    /**
                    * Disable next button of form wizard if campaign input is blank
                    *
                    */

                    $("#campaign_name").keyup(function(){

                        var campaign_name = $(this).val();

                        if(/^[^\s].*/.test(campaign_name.trim())){

                            $(".btn-save-campaign-draft button").removeAttr('disabled');

                            enableWizardNext();
                        }
                        else{

                            $(".btn-save-campaign-draft button").attr('disabled', 'disabled');

                            disableWizardNext();
                        }
                    });

                    /**
                    * Action for message body counter
                    *
                    */

                    function getCounter(len){

                        if(len == 0){

                            $("#charCounter").text("0");
                        }
                        else if (len >= 1 && len <= 160){

                            $("#charCounter").text("1");
                        
                        }else if(len >= 161 && len <= 306){
                            
                            $("#charCounter").text("2");
                        }
                        else{
                            
                            $("#charCounter").text("3");
                        }
                    }

                    /**
                    * Action for disabling schedule date and time if send now checkbox is click
                    *
                    */

                    $("input[name=check-send-now]").click(function(){
                        $(".form_schedule_date").html("Send Immediately");
                        if($(this).is(":checked")){
                            $("#timepicker").attr('disabled', 'disabled');
                            $("#timepicker").css('background', '#d9d9d9');
                            $("#timepicker").removeClass('error');
                            $("#timepicker").parent().find('#datepicker-error').remove();
                            
                            $("#datepicker").attr('disabled', 'disabled');
                            $("#datepicker").css('background', '#d9d9d9');
                            $("#datepicker").removeClass('error');
                            $("#datepicker").parent().find('#datepicker-error').remove();

                            //enable wizard next button
                            $('.actions > ul > li:nth-child(3) > a').removeAttr('class');
                            $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
                            $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish'); 
                            $('.actions > ul > li:nth-child(3)').removeClass('disabled');
                        }
                        else{
                            $("#datepicker").removeAttr('disabled');
                            $("#datepicker").css('background', '#fff');

                            $("#timepicker").removeAttr('disabled');
                            $("#timepicker").css('background', '#fff');

                            //disable wizard next button
                            $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                            $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
                            $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
                            $('.actions > ul > li:nth-child(3)').addClass('disabled');
                        }
                    });


                    /**
                    * Action for enabling schedule date and time input if clear button is click
                    *
                    */

                    $(".remove-schedule").click(function(){

                        if($("input[name=check-send-now]").is(":checked")){

                            return false;
                        }

                        $("#datepicker").val("");
                        $("#timepicker").val("");
                        $(".form_schedule_time").html("");
                        $("#form_schedule").val("");
                        $("#timepicker").css("border", "1px solid #ccc");
                        $("#datepicker").css("border", "1px solid #ccc");

                        //constantly check the time for disabling send now checkbox
                        getSendNowStat();

                        $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                        $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
                        $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
                        $('.actions > ul > li:nth-child(3)').addClass('disabled');
                    });

                    // Tooltip for clipboard
                    $('.copyclip').tooltip({
                      trigger: 'click',
                      placement: 'bottom'
                    });

                    //copy to clipboard
                    var clipboard = new Clipboard('.copyclip');

                    clipboard.on('success', function(e) {
                      setTooltip(e.trigger, 'Copied!');
                      hideTooltip(e.trigger);
                    });

                    clipboard.on('error', function(e) {
                      setTooltip(e.trigger, 'Failed!');
                      hideTooltip(e.trigger);
                    });

                    $("#broadcast-container").show();


                    /**
                    * Action for selecting contact groups from first step of form wizard
                    *
                    */
                    $("#broadcast-container").on('click', 'input[name=contact_group]', function(){
                        
                        contactGroupsAction($(this));
                        contactGroupsValues('single');
                    });


                    /**
                    * Action for contact groups multiple select from first step of form wizard
                    *
                    */
                    $("#broadcast-container").on('click', '#all-contact-lists', function(){
                        checkedValues = [];
                        $('input[name="contact_group"]').not(this).prop('checked', this.checked);
                        $('input[name="contact_group"]').each(function() {
           
                            contactGroupsAction($(this));
                        });
                        contactGroupsValues('multiple');
                    });

                    /**
                    * Saving of contact group ids in array
                    * @param {element} elem - checkbox input
                    */
                    function contactGroupsAction(elem){
                        var data = new Array();

                        if(elem.is(':checked')){
                            elem.next("label").removeClass("clr");
                            elem.next("label").addClass("chk");
                            var count = elem.parent().parent().find('.total-base-number').html();
                            count = count.replace(/, /g,'');
                            var checkboxText = elem.parent().parent().find('.css-label').html();
                            data['id'] = elem.val();
                            data['total_value'] = count;
                            data['name'] = checkboxText;
                            checkedValues.push(data);
                            contactIds.push(parseInt(elem.val()));
                        }
                        else{
                            elem.next("label").addClass("clr");
                            elem.next("label").removeClass("chk");
                            for (var i = 0; i < checkedValues.length; i++)
                            {
                                if (checkedValues[i].id == elem.val())
                                {
                                    checkedValues.splice(i, 1);
                                    break;
                                }
                            }
                            for (var i = 0; i < contactIds.length; i++)
                            {
                                if (contactIds[i] == elem.val())
                                {
                                    contactIds.splice(i, 1);
                                    break;
                                }
                            }
                        }
                    }

                    /**
                    * Manipulation of contact groups ids 
                    * @param {string} action - action from selecting single contact group or multiple contact groups
                    */
                    function contactGroupsValues(action){
                        
                        /**
                        * sort contact groups name alphabetically
                        * @param {integer} stringSort - return placement of string
                        *
                        */
                        checkedValues.sort(stringSort);

                        strContact = "";
                        if(checkedValues.length > 0){

                            /*container for selected contact groups in broadcast summary modal*/
                            $(".selected-audience").html("");

                            var total_count = 0;

                            /*append total base count for each contact groups in broadcast summary modal*/
                            for(var i in checkedValues){
                                $(".selected-audience").append(checkedValues[i].name+", ");
                                total_count = parseInt(total_count) + parseInt(checkedValues[i].total_value);
                            }

                            //step1
                            $(".total-all-contacts").html(total_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                            $(".total_audience").html("Total: "+total_count.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
                            
                            //summary
                            var selected_audience = $(".selected-audience").html();
                            selected_audience = selected_audience.substr(0, selected_audience.length - 2);

                            var selected_action = 'All Contact Groups';
                            if(action == 'single'){

                                selected_action = selected_audience;
                            }
                            $(".selected-audience").html(selected_action);
                            //form submit
                            $("input[name=form_contact_group]").val(contactIds);

                            //enable wizard next button
                            enableWizardNext();
                        }
                        else{
                            $(".total-all-contacts").html("0");
                            $(".total_audience").html("");
                            $(".selected-audience").html("");

                            disableWizardNext();
                        }
                    }

                    return true;
                },
                onStepChanging: function(event, currentIndex, newIndex){

                    // If user click on "Previous" button, we just normally let he/she goes
                    if (newIndex < currentIndex) {
                        $('.actions > ul > li:nth-child(3)').css('display', 'none');
                        enableWizardNext();
                        return true;
                    }
                    else{

                        if($("#campaign_name").val() != "" || $("#broadcast_message").val() != ""){

                            enableWizardNext();
                        }
                        else{
                            disableWizardNext();
                        }

                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    }
                },
                onStepChanged: function (event, currentIndex, priorIndex){

                    $('.wizard>.actions>ul').css('margin-left', '0px');

                    if (currentIndex > 0) {
                        $('.actions > ul > li:first-child').attr('style', '');

                        if($("#form_schedule").val() && $("#form_schedule").val() != 'None'){

                            var utcSeconds = $("#form_schedule").val();
                            var date = new Date(0);
                            date.setUTCSeconds(utcSeconds);

                            $("input[name=form_schedule]").val(utcSeconds);
                            $("#datepicker").datepicker('setDate', date);
                            $('#timepicker').timepicker('setTime', date);

                        }

                        if($(".btn-save-campaign-draft button").data("draft") && $("#form_schedule").val() && $("#form_schedule").val() != "None"){

                            isPaused = true; //pause interval for send now button
                            $("input[name=check-send-now]").attr('disabled', 'disabled');
                            $(".sched-now").css('color', '#d9d9d9');
                        }
                        } else {
                            $('.actions > ul > li:first-child').attr('style', 'display:none');
                        }

                      if($(".btn-save-campaign-draft button").data('draft')){

                            $(".btn-save-campaign-draft button").removeAttr('disabled');
                        }

                    //disable finish button
                    if(currentIndex == 2){

                        if(/^[^\s].*/.test($("#campaign_name").val().trim())){

                            if(($("input[name=form_schedule]").val() != "" && $("input[name=form_schedule]").val() != 'None') || $("input[name=check-send-now]").is(":checked")){

                                $('.actions > ul > li:nth-child(3)').css('display', 'block');
                                $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                                $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
                                $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish');
                                $('.actions > ul > li:nth-child(3)').removeClass('disabled');
                            }
                            else{

                                $('.actions > ul > li:nth-child(3)').css('display', 'block');
                                $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                                $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
                                $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
                                $('.actions > ul > li:nth-child(3)').addClass('disabled');
                            }
                        }
                        else{
                            element.steps('previous');
                        }
                    }

                    /**
                    * Change form wizard content height
                    * index 0 - step 1
                    * index 1 - step 2
                    * index 2 - step 3
                    *
                    */
                    if(currentIndex == 0){

                        $("#broadcast-container .content").css('min-height', '26em');
                    }
                    else if(currentIndex == 1){

                        $("#broadcast-container .content").css('min-height', '35em');
                    }
                    else{

                        $("#broadcast-container .content").css('min-height', '31em');
                    }

                    getDisabledTime();

                    /**
                    * Re-initialize datepicker when user click the key esc
                    *
                    */
                    $("#datepicker").keyup(function(e) {
                        if (e.keyCode == 27) {
                            $("#datepicker").datepicker("show");
                        }
                    });
                    
                    /**
                    * Initialize datepicker
                    *
                    */
                    $("#datepicker").datepicker({
                        dateFormat: 'mm/dd/y',
                        minDate: minDate,
                        onSelect: function(selectedDate){
                            $(".form_schedule_date").html("<strong>Date: </strong>"+ selectedDate);
                            $("input[name=schedule_date]").next('label.error').remove();
                            $("input[name=schedule_date]").removeClass('error');
                            isPaused = true; //pause interval for send now button
                            $("input[name=check-send-now]").attr('disabled', 'disabled');
                            $(".sched-now").css('color', '#d9d9d9');
                            $("#timepicker").removeAttr('disabled');
                            $("#timepicker").css('background', '#fff');

                            var current_date = new Date();
                            var selected_date = new Date(selectedDate);

                            /**
                            * Set disabled time variable for timepicker
                            *
                            */
                            if(selected_date > current_date){

                                strdisbaledTime = "7am";
                            }
                            else{

                                getDisabledTime();
                            }

                            /**
                            * Re-initialize timepicker based on selected date
                            *
                            */
                            $('#timepicker').timepicker('option', {'disableTimeRanges': [
                                ['7am', strdisbaledTime]
                            ]});

                            if($('#timepicker').val() != ""){

                                //save epoch time
                                var epoch_time = getEpochTime();
                                $("input[name=form_schedule]").val(epoch_time);
                            }

                            $('#datepicker').css("border", "1px solid #ccc");

                            if($('#timepicker').val() == ""){

                                /*disable wizard next button*/
                                $('.actions > ul > li:nth-child(3)').css('display', 'block');
                                $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                                $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
                                $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
                                $('.actions > ul > li:nth-child(3)').addClass('disabled');
                            }
                            else{

                                //enable wizard next button
                                $('.actions > ul > li:nth-child(3) > a').removeAttr('class');
                                $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
                                $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish'); 
                                $('.actions > ul > li:nth-child(3)').removeClass('disabled');
                            }

                        }
                    }).focus(function(){

                        $('#timepicker').val("");
                    });
           
                    $('#timepicker').timepicker({
                        'disableTimeRanges': [
                            ['7am', strdisbaledTime]
                        ],
                        'className': 'timepick',
                        'disableTextInput': true,
                        timeFormat: 'h:i A',
                        step: 15,
                        minTime: '08',
                        maxTime: '6:00pm',
                        dropdown: true
                    });

                    $('#timepicker').on('changeTime', function() {

                        $(".form_schedule_time").html("<strong>Time: </strong>"+ $(this).val());
                        $("input[name=check-send-now]").attr('disabled', 'disabled');
                        $(".sched-now").css('color', '#d9d9d9');
                        isPaused = true; //pause interval for send now button

                        if($("#datepicker").val() != ""){

                            //save epoch time
                            var epoch_time = getEpochTime();
                            $("input[name=form_schedule]").val(epoch_time);
                        }

                        $('#timepicker').css("border", "1px solid #ccc");

                        if($('#datepicker').val() == ""){

                            /*disable wizard next button*/
                            $('.actions > ul > li:nth-child(3)').css('display', 'block');
                            $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
                            $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
                            $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
                            $('.actions > ul > li:nth-child(3)').addClass('disabled');
                        }
                        else{

                            //enable wizard next button
                            $('.actions > ul > li:nth-child(3) > a').removeAttr('class');
                            $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
                            $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish'); 
                            $('.actions > ul > li:nth-child(3)').removeClass('disabled');
                        }
                    });

                },
                onFinishing: function (event, currentIndex) 
                { 
                    $(".campaign-body").html($("#campaign_name").val()).text();

                    var epoch_time = getEpochTime();

                    if($("input[name=check-send-now]").is(":checked")){
                        $("input[name=form_schedule]").val(null);

                        $("#broadcast-summary").modal({backdrop: 'static',
                                                keyboard: false});
                    }
                    else{

                        if($("#timepicker").val() == ""){

                            $("#timepicker").css("border", "1px solid red");
                            $("#datepicker").css("border", "1px solid #ccc");

                            return false;
                        }
                        else if($("#datepicker").val() == ""){
                            $("#datepicker").css("border", "1px solid red");
                            $("#timepicker").css("border", "1px solid #ccc");

                            return false;   
                        }
                        else{

                            $("#datepicker").css("border", "1px solid #ccc");
                            $("#timepicker").css("border", "1px solid #ccc");

                            $("input[name=form_schedule]").val(epoch_time);
                            //show date in summary
                            $(".form_schedule_date").html("<strong>Date: </strong>"+ $("#datepicker").val());

                            $("#broadcast-summary").modal({backdrop: 'static',
                                                keyboard: false});
                        }
                    }

                    return false;
                    
                },
                onFinished: function (event, currentIndex){
                    
                }
            });
            return {
                //pre-link
                pre:function() {},
                //post-link
                post: function(scope, element) {
                    element.on('stepChanging', scope.stepChanging);
                }
            }
        }
    };
}]);

app.controller('Ctrl', ['$scope', 'dataFactory', function($scope, dataFactory) {

    $scope.contact_groups;
    $scope.status;

    getContactGroups();
    function getContactGroups(){
        dataFactory.getContactGroups().then(function (response) {
            $scope.contact_groups = response.data.contact_groups;
        }, function (error) {
            
        });
    }

}]);


$("#send_live_bcast").click(function() {

    stopAnimate = false;
    //$("#loading").html('Submitting');

    var data = {
        contact_list_id: contactIds,
        campaign_name: $("#campaign_name").val().toUpperCase(),
        campaign_id: $("#campaign_id").val(),
        message: $("#broadcast_message").val(),
        sender_id: $("#select-sender-id").val(),
        schedule: $("input[name=form_schedule]").val() || null
    }
    
    $("#loading").css('display', 'block');

    $(".btn-submit-broadcast").attr('disabled', 'disabled');
    $(".btn-back").attr('disabled', 'disabled');
    $(".error-broadcast").hide();

    $.ajax({
        method: "POST",
        url: "/broadcasts/do_live/",
        data: JSON.stringify(data),
        contentType: "application/json",
        headers: {'X-CsrfToken': csrf}
    }).done(function(msg) {
        // Debug.
        var ref_id = msg.ref_id;
        $(".btn-save-campaign-draft").hide();
        $("#broadcast-summary").modal("hide");
        $("#reference_number").val(ref_id);
        $("#result-campaign-name").html($("#campaign_name").val());
        $("#broadcast-controller").remove();
        $(".modal-backdrop").remove();
        $("#broadcast-result").show();
    }).fail(function(jqXHR, textStatus){
        
        i = 4;
        stopAnimate = true;
        $("#loading").css('display', 'none');

        $(".btn-submit-broadcast").removeAttr('disabled');
        $(".btn-back").removeAttr('disabled');

        $(".error-broadcast").css('display', 'block');

        try{

            var error = jqXHR.responseText;
            var data_error = $.parseJSON(error);
            
            var error_text = "";
            if(data_error.code == '001' || data_error.code == '002'){

                $(".btn-submit-broadcast").attr('data-error', 'invalid_data');
                error_text = "We noticed you have invalid data submitted";
            }
            else if(data_error.code == '005'){

                $(".btn-submit-broadcast").attr('data-error', 'invalid_schedule_time');
                error_text = "Can't set schedule earlier than current time.";
            }
            else if(data_error.code == '012'){

                $(".btn-submit-broadcast").attr('data-error', 'invalid_campaign');
                error_text = "<strong>"+$("#campaign_name").val() + "</strong> already exists";
            }
           else if(data_error.code == '007' || data_error.code == '003'){
            //call to sender api/broadcast failed
            $(".btn-submit-broadcast").removeAttr('disabled');
                $(".btn-back").removeAttr('disabled');
                //$(".btn-submit-broadcast").attr('data-error', 'invalid_schedule');
                error_text = "Campaign broadcast failed. Please try again.";
            }
            else if(data_error.code == '006'){

                $(".btn-submit-broadcast").attr('data-error', 'invalid_schedule');
                error_text = "Sender ID is not yet approved";
            }

            $(".error-broadcast").find('.error-text').html(error_text);
        }
        catch(error){

            $(".error-broadcast").find('.error-text').html("Campaign broadcast failed. Please contact Rising Tide");
        }
        
    });
});

$("#broadcast-container").on("click", ".btn-search-groups", function(){

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });

    var inputString = $("#inputString").val();

    var data = {
        search_string: inputString
    }

    $.ajax({
        url: "/contacts/search/",
        type: "post",
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken},
        data: JSON.stringify(data),
        success: function(result){

            $.LoadingOverlay("hide");

            var data = result.contact_groups;

            $("#wcontact-lists .checkbox").remove();

            for(val in data){
                
                $("#wcontact-lists").append('<div class="checkbox" data-id="'+data[val].id+'"><div style="float:left;width:60%"><input type="checkbox" name="contact_group" value="'+data[val].id+'" id="dummycontact-'+data[val].id+'" class="css-checkbox" /><label for="dummycontact-'+data[val].id+'" class="css-label contact-item radGroup'+data[val].id+'"> '+data[val].name+'</label></div><div style="float:right;width:40%;"><p>(base: <span class="total-base-number">'+commaSeparateNumber(data[val].base_count)+'</span>)</p></div></div>');
            }
        }   
    });
});

/*
 //KILL DEBUGGER
$(document).on("contextmenu",function(e){        
   e.preventDefault();
});

$(document).keydown(function(event){
    if(event.keyCode==123) {
        return false;
    } else if(event.ctrlKey && event.shiftKey && event.keyCode==73) {        
      return false;  //Prevent from ctrl+shift+i
    }
});
*/