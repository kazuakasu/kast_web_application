$("#header-navigation").sticky({ topSpacing: 0 });

var form = $("#login-form");

form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        email: {
            email: true
        }
    },
    messages: {
        email: {
            email: "Please input a valid email address"
        }
    },
    submitHandler: function(form) {
        $.ajax({
            type: "POST",
            data: $("#login-form").serialize(),
            dataType: 'json',
            url: '/home/',
            success: function (data) {
                if(data.status == "ok"){
                    window.location.replace(data.url);
                } else {
                   var label = '<label class="error" style="margin-top:1em">Invalid email or password</label>';
                   $("#login-form .form-group:first-of-type").append(label);
                   $("#login-form #email").addClass("error");
                   $("#login-form #password").addClass("error");
                }
            }
        });
    }
});

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('#login-form input:not([type="submit"])').each(function() {
	console.log("3");
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        } 
    });

    if (empty) {
        $('#login-form .btn-submit').attr('disabled', 'disabled');
    } else {
        $('#login-form .btn-submit').removeAttr('disabled');
    }
});

(function() {
    $('#login-form input:not([type="submit"])').on("change keyup blur mouseenter", function() {
        var empty = false;
        $('#login-form input:not([type="submit"])').each(function() {
	    console.log("3");
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('#login-form .btn-submit').attr('disabled', 'disabled');
        } else {
            $('#login-form .btn-submit').removeAttr('disabled');
        }
    });
})()
