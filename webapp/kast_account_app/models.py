"""

This class contains the Kast Accounts Django models. It defines database fields of Kast Tenants
and Kast Users

Models:
    KastUsers

"""

from django.db import models
from django.contrib.auth.models import User
from django_extensions.db.fields import ShortUUIDField
from django.db.models.signals import post_save
from django.dispatch import receiver

# Tenant Data Model
class KastTenants(models.Model):

    """ Kast Tenants Model """

    uid         = ShortUUIDField(unique=True)
    name        = models.CharField(max_length=30, unique=True)
    is_active   = models.BooleanField(default=True, db_index=True)
    created     = models.DateTimeField(auto_now_add=True)
    modified    = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "kast_tenants"

    def __unicode__(self):

        """ returns tenant name in Django admin """

        return self.name

# User Data Model 
class KastUsers(models.Model):

    """ Kast Users Model """

    CHOICES = (
        ('trial',  'trial'),
        ('basic',  'basic'),
        ('premium','premium')
    )

    uid               = ShortUUIDField(unique=True)
    user              = models.OneToOneField(User, on_delete=models.CASCADE)
    name              = models.CharField(null=True, max_length=60)
    tenant            = models.CharField(null=True, max_length=30)
    activation_key    = models.CharField(null=True, unique=True, max_length=30)
    contact           = models.CharField(null=True, max_length=15)
    status            = models.CharField(max_length=30, choices=CHOICES, default='trial')
    is_trial_consumed = models.BooleanField(default=False)
    created           = models.DateTimeField(auto_now_add=True)
    modified          = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "kast_users"

    def __unicode__(self):

        """ returns user email in Django admin """

        return self.user.email

    def get_tenant(self):

        """ returns the user's tenant """

        return KastTenants.objects.get(name=self.tenant)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):

    """
    
    Signal to create Kast user when Django auth user table has a new entry

    :type sender: obj
    :param sender: Django auth user object

    :type instance: obj
    :param instance: Copy of the saved user model instance

    :type created: bool
    :param created: Check if a new user has been created

    :type **kwargs: dict
    :param **kwargs: Keyword arguments

    :rtype: None
    :return: Nothing

    """

    if created:
        KastUsers.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):

    """
    
    Signal to save/update Kast user when Django auth user table has been saved/updated

    :type sender: obj
    :param sender: Django auth user object

    :type instance: obj
    :param instance: Copy of the saved user model instance

    :type **kwargs: dict
    :param **kwargs: Keyword arguments

    :rtype: None
    :return: Nothing

    """
    
    instance.kastusers.save()

