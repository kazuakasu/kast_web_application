"""

This module is responsible for rendering home pages

Pages rendered:
    index
    login
    faqs
    contact us
    about us
    pricing
    documentation
    case studies

Models used:
    KastTenants

"""

from django.shortcuts import render
from django.shortcuts import redirect

from django.http import HttpResponse
from django.core.mail import send_mail

from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.sites.shortcuts import get_current_site

from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.csrf import csrf_exempt

from kast_account_app.models import KastTenants

import json

@never_cache
@csrf_exempt
def home(request):

    """

    Renders home page or handles redirection

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered home page, error or redirects to dashboard page

    """

    # Render the home page as starting point
    if (request.method == "POST"):
        form = AuthenticationForm(data=request.POST)
        if (form.is_valid()):
            guest = authenticate(username = request.POST.get('username'), password = request.POST.get('password'))
            if (guest is not None):
                # Identify the will be login type.
                user = User.objects.get(email = request.POST.get('username'))
                if (user.kastusers.tenant is not None):
                    # User has a tenant already
                    tenant = KastTenants.objects.get(name=user.kastusers.tenant)
                    if (tenant.is_active == True):
                        # Tenant is still active. Set user login type
                        if (user.kastusers.status != 'trial'):
                            login(request, guest)
                            request.session['login_type'] = 'live'
                            response_dict = {}
                            response_dict['status'] = "ok"
                            response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                            return HttpResponse(json.dumps(response_dict))
                        else:
                            login(request, guest)
                            request.session['login_type'] = 'trial'
                            response_dict = {}
                            response_dict['status'] = "ok"
                            response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                            return HttpResponse(json.dumps(response_dict))
                    else:
                        # Tenant was deactivated
                        response_dict = {}
                        response_dict['status']  = "error"
                        response_dict['message'] = "The company in which this account belongs has been deactivated"
                        return HttpResponse(json.dumps(response_dict))
                else:
                    # User has no tenant
                    login(request, guest)
                    request.session['login_type'] = 'trial'
                    response_dict = {}
                    response_dict['status'] = "ok"
                    response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                    return HttpResponse(json.dumps(response_dict))
            else:
                response_dict = {}
                response_dict['status']  = "error"
                response_dict['message'] = "Account does not exist"
                return HttpResponse(json.dumps(response_dict))
        else:
            response_dict = {}
            response_dict['status']  = "error"
            response_dict['message'] = "Account does not exist"
            return HttpResponse(json.dumps(response_dict))
    else:
        if (request.user.is_authenticated()):
            return redirect('/dashboard')
        else: 
            return render(request, 'kast_home_app_templates/home.html')

@never_cache
@csrf_exempt
def home_login(request):

    """

    Renders login page and handles redirection

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered login page, error or redirects to dashboard page

    """

    if (request.method == "POST"):
        form = AuthenticationForm(data=request.POST)
        if (form.is_valid()):
            guest = authenticate(username = request.POST.get('username'), password = request.POST.get('password'))
            if (guest is not None):
                # Identify the will be login type.
                user = User.objects.get(email = request.POST.get('username'))
                if (user.kastusers.tenant is not None):
                    # User has a tenant already
                    tenant = KastTenants.objects.get(name=user.kastusers.tenant)
                    if (tenant.is_active == True):
                        # Tenant is still active. Set user login type
                        if (user.kastusers.status != 'trial'):
                            login(request, guest)
                            request.session['login_type'] = 'live'
                            response_dict = {}
                            response_dict['status'] = "ok"
                            response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                            return HttpResponse(json.dumps(response_dict))
                        else:
                            login(request, guest)
                            request.session['login_type'] = 'trial'
                            response_dict = {}
                            response_dict['status'] = "ok"
                            response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                            return HttpResponse(json.dumps(response_dict))
                    else:
                        # Tenant was deactivated
                        response_dict = {}
                        response_dict['status']  = "error"
                        response_dict['message'] = "The company in which this account belongs has been deactivated"
                        return HttpResponse(json.dumps(response_dict))
                else:
                    # User has no tenant
                    login(request, guest)
                    request.session['login_type'] = 'trial'
                    response_dict = {}
                    response_dict['status'] = "ok"
                    response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/dashboard")
                    return HttpResponse(json.dumps(response_dict))
            else:
                response_dict = {}
                response_dict['status']  = "error"
                response_dict['message'] = "Account does not exist"
                return HttpResponse(json.dumps(response_dict))
        else:
            response_dict = {}
            response_dict['status']  = "error"
            response_dict['message'] = "Account does not exist"
            return HttpResponse(json.dumps(response_dict))
    else:
        if (request.user.is_authenticated()):
            return redirect('/dashboard')
        else: 
            return render(request, 'kast_home_app_templates/home-login.html')

def faqs(request):

    """
    
    Renders FAQ page. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered FAQs page

    """

    # render faqs page
    if (request.user.is_authenticated()):
        status = request.session['login_type']
        return render(request, 'kast_home_app_templates/faqs.html', {'p_status': status})
    else:
        return render(request, 'kast_home_app_templates/faqs.html')

def pricing(request):

    """
    
    Renders pricing page. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered pricing page

    """

    # render pricing page
    return render(request, 'kast_home_app_templates/pricing.html')

@csrf_exempt
def contact_us(request):

    """
    
    Renders contact us page and sends an email to Kast admin. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered contact us page or http response object

    """

    if (request.method == "POST"):
        name    = request.POST.get('name')
        email   = request.POST.get('email')
        subject = request.POST.get('subject')
        message = request.POST.get('message')

        subject = 'KAST | '+str(subject)

        msg = '\n\nName: '+str(name)+'\n\nEmail: '+str(email)+'\n\nMessage: '+str(message)

        # Send the message to kast_admin
        send_mail(subject, msg, email, ['switch@risingtide.ph'], fail_silently=False)
        response_dict = {}
        response_dict['status'] = "ok"
        return HttpResponse(json.dumps(response_dict))

    else:
        # render contact-us page
        return render(request, 'kast_home_app_templates/contact-us.html')

def about_us(request):

    """
    
    Renders about us page. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered about us page

    """

    # render about-us page
    return render(request, 'kast_home_app_templates/about-us.html')

def documentation(request):

   """
    
    Renders documentation page. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered documentation page

    """

    # render documentation page
    return render(request, 'kast_home_app_templates/documentation.html')

def case_studies(request):

    """
    
    Renders case studies page. 

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered case studies page

    """

    # render case-studies page
    return render(request, 'kast_home_app_templates/case-studies.html')
