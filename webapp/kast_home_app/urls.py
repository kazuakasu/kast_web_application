from django.conf.urls import url
from django.views.generic import RedirectView
from . import views

urlpatterns = [

    # Route to KAST landing page login
    url(r'^$', RedirectView.as_view(url='/home', permanent=False)),
    url(r'^home/$', views.home, name='home'),
    url(r'^login/$', views.home_login, name='login'),
    # Route to KAST landing page sidemenu extensions
    url(r'^pricing/$', views.pricing, name='pricing'),
    # url(r'^contact-us/$', views.contact_us, name='contact-us'),
    url(r'^faqs/$', views.faqs, name='faqs'),
    # url(r'^about-us/$', views.about_us, name='about-us'),

    # Disabled links
    #url(r'^documentation/$', views.documentation, name='documentation'),
    #url(r'^case-studies/$', views.case_studies, name='case-studies'),
]
