"""

This module is mainly used for the broadcast section of Kast. Most of the functions written
are for validation purposes. 

Models used:
    KastSIDs
    KastUserCampaigns  

"""

import json
import redis
import logging
from contextlib import closing
from zipfile import ZipFile
from os.path import basename

import requests
from requests.auth import HTTPBasicAuth
import arrow
from hashids import Hashids
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import send_mail

from webapp.middleware import KastApiError
from webapp.util import phonenumbers_ph as phone_ph
from kast_sender_id_app.models import KastSIDs
from kast_campaign_app.models import KastUserCampaigns

util_redis = redis.StrictRedis(
    host='localhost',
    port=6379,
    socket_timeout=5
)

logger = logging.getLogger('django.server')


def clean_data(body, required_params=None):

    """

    Performs UTF-8 decode on passed JSON object.

    :type body: obj 
    :param body: The JSON object to be decoded. Usually Django's 'request' object.

    :type required_params: list
    :param required_params: List of model parameters. (optional)

    :rtype: obj
    :return: UTF-8 decoded JSON object

    :raise KastApiError 001: Invalid request body.
    :raise KastApiError 002: Missing required parameters.

    """

    try:
        try:
            data = json.loads(body.decode('utf-8'))
            filter(lambda _: data[_], required_params or [])
        except:
            data = {}
            for param in required_params:
                data[param]=body.get(param)
    except ValueError as e:
        logger.exception(e)
        raise KastApiError(
            {'error': 'Invalid request body.', 'code': '001'}, 400)
    except KeyError as e:
        logger.exception(e)
        raise KastApiError(
            {'error': 'Missing required parameters.', 'code': '002'}, 400)

    return data


def clean_schedule(schedule_ts=None):

    """

    Validates a scheduled broadcast if said schedule is within broadcasting time

    .. note::
        If parameter 'schedule_ts' is 'None', broadcast will be executed immediately.

    :type schedule: str
    :param schedule: the given schedule of a campaign

    :rtype: str or None
    :return: valid schedule in datetime format or nothing

    :raise KastApiError 005: Broadcast schedule is not between bradcasting period.   

    """

    schedule = arrow.get(schedule_ts).to('Asia/Manila')
    start = arrow.get(schedule).replace(hour=8).floor('hour')
    end = arrow.get(start).replace(hour=18)

    if not (start.timestamp <= schedule.timestamp <= end.timestamp):
        msg = 'Please schedule broadcast between %s - %s.' %\
            (start.format('HH:mm'), end.format('HH:mm'))
        raise KastApiError({'error': msg, 'code': '005'}, 400)

    return schedule_ts


def do_broadcast(tenant, message, csv,
                 user_id, schedule=None, sender_id=None):

    """

    Execute immediate or scheduled broadcast with or without a given sender ID.

    .. note::
        If parameter 'schedule' is 'None', broadcast will be executed immediately.
        If parameter 'sender_id' is 'None', the default sender ID will be used.

    :type tenant: str
    :param tenant: Tenant name
    
    :type message: str
    :param message: text message of the broadcast

    :type csv: str
    :param csv: file containing list of MINs to be broadcasted

    :type user_id: int
    :param user_id: logged in user ID used for generating a reference code

    :type schedule: str
    :param schedule: the given schedule of a campaign (immediate if NULL)

    :type sender_id: str
    :param sender_id: approved sender ID (default sender ID if NULL) 

    :rtype: str
    :return: broadcast reference code 

    .. warning::
        *callback* URL is hardcoded

    :raise KastApiError 003: Broadcast API hit fail.

    """

    conf = settings.BROADCASTS
    auth = HTTPBasicAuth(conf['user'], conf['pass'])

    try:
        tenant_name = tenant.replace(" ","_")
        do_bcast = requests.post(conf['base_url'], auth=auth, json={
            'billing_name':tenant_name,
            'sender_id': sender_id or conf['sender_id'],
            'message': message,
            'members': csv,
            'schedule': schedule,
            'callback': 'http://www.kast.ph/broadcasts/callback/'
            })
        logger.info('Call to broadcast api returned: %s' % do_bcast.text)

        # Raise exception when status code not ok.
        do_bcast.raise_for_status()

        # Raise ValueError if json cannot be decoded.
        res = do_bcast.json()
        broadcast_id = res['broadcast_id']
    except (requests.exceptions.HTTPError, KeyError, ValueError) as e:
        logger.exception(e)
        raise KastApiError(
            {'error': 'Call to broadcast api failed.', 'code': '003'}, 500)

    # WANT: Put in project util.
    h = Hashids(
        salt='KastApp@2017',
        min_length=12,
        alphabet='ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890')
    ref_id = h.encode(user_id, broadcast_id)

    return ('SBC-%s' % ref_id, broadcast_id)


def do_resend(tenant, message, csv,
                 bcast_id, schedule=None, sender_id=None):

    """

    Re-executes live broadcast in case the live broadcast fails

    .. note::
        If parameter 'schedule' is 'None', broadcast will be executed immediately.
        If parameter 'sender_id' is 'None', the default sender ID will be used.

    :type tenant: str
    :param tenant: Tenant name
    
    :type message: str
    :param message: text message of the broadcast

    :type csv: str
    :param csv: file containing list of MINs to be broadcasted

    :type bcast_id: int
    :param bcast_id: broadcast ID of failed live broadcast

    :type schedule: str
    :param schedule: the given schedule of a campaign (immediate if NULL)

    :type sender_id: str
    :param sender_id: approved sender ID (default sender ID if NULL) 

    :rtype: str
    :return: broadcast reference code 

    .. warning::
        *callback* URL is hardcoded
        Escalate this function to main developer

    :raise KastApiError 003: Broadcast API hit fail.

    """

    conf = settings.BROADCASTS
    auth = HTTPBasicAuth(conf['user'], conf['pass'])

    try:
        tenant_name = tenant.replace(" ","_")
        do_bcast = requests.post(conf['base_url']+'resend/',
            auth=auth, json={
                'bcast_id':bcast_id,
                'billing_name':tenant_name,
                'sender_id': sender_id or conf['sender_id'],
                'message': message,
                'members': csv,
                'schedule': schedule,
                'resend': arrow.now("Asia/Manila").strftime("%y%m%d%H%M%S"),
                'callback': 'http://www.kast.ph/broadcasts/callback/'
            })
        logger.info('Call to broadcast api returned: %s' % do_bcast.text)

        # Raise exception when status code not ok.
        do_bcast.raise_for_status()

        # Raise ValueError if json cannot be decoded.
        res = do_bcast.json()
        broadcast_id = res['broadcast_id']
    except (requests.exceptions.HTTPError, KeyError, ValueError) as e:
        logger.exception(e)
        raise KastApiError(
            {'error': 'Call to broadcast api failed.', 'code': '003'}, 500)

    return ('SBC-%s' % broadcast_id)


def clean_sender_id(id):

    """

    Validates sender ID.

    .. note::
        The sender ID must be approved by the **admin** AND must have no duplicates.

    :type id: int
    :param id: ID integer value of the sender ID

    :rtype: str
    :return: valid sender ID

    .. warning::
        URL for checking approved sender ID is hardcoded

    :raise KastApiError 006: Sender ID not approved.
    :raise KastApiError 007: Sender ID API hit fail.
    :raise KastApiError 010: No sender ID.

    """

    try:
        SID = KastSIDs.objects
        sender_id = SID.get(pk=id).name
        return sender_id
    except KastSIDs.DoesNotExist as e:
        logger.exception(e)
        raise KastApiError({
            'error': 'Sender ID #%s not found.' % id,
            'code': '010'}, 404)

    try:
        # TODO: Put to conf.
        url = 'http://172.30.0.44/senderid/'
        do_check = requests.get(url+sender_id)
        logger.info('Call to sender id api returned: %s' % do_check.text)

        # Raise exception when status code not ok.
        do_check.raise_for_status()

        # Raise ValueError if json cannot be decoded.
        res = do_check.json()
        status = res['status']
    except (requests.exceptions.HTTPError, KeyError, ValueError) as e:
        logger.exception(e)
        raise KastApiError(
            {'error': 'Call to sender id api failed.', 'code': '007'}, 500)

    # Check if sender id is approved.
    if status != 1:
        raise KastApiError({
            'error': 'Sender ID: %s not approved' % sender_id,
            'code': '006'}, 400)

    return sender_id


def clean_campaign_name(campaign_id, campaign_name, user):

    """

    Validates campaign name input.

    .. note::
        The campaign name must have no duplicates

    :type campaign_id: int
    :param campaign_id: ID integer value of the campaign

    :type campaign_name: str
    :param campaign_name: name of the campaign

    :type user_id: int
    :param user_id: user ID of the campaign creator

    :rtype: str
    :return: valid campaign name

    :raise KastApiError 012: Campaign name already exists

    """

    try:
        campaign = KastUserCampaigns.objects.get(
            campaign_name=campaign_name,
            campaign_type='sms_broadcast',
            user=user)
    except KastUserCampaigns.DoesNotExist:
        return campaign_name

    try:
        cid = int(campaign_id)
    except ValueError:
        cid = None

    if cid != campaign.id:
        raise KastApiError({
            'error': 'Campaign name: %s already exists.' % campaign_name,
            'code': '012'}, 400)
    return campaign_name


def download_file(url, filename):
    reports = settings.BROADCASTS['report_url']
    with closing(requests.get(reports+url, stream=True)) as r:
        if r.encoding is None:
            r.encoding = 'utf-8'

        filepath = '/tmp/%s' % filename.replace("/","_")
        with open(filepath, 'wb') as f:
            for chunk in r.iter_content(
                chunk_size=None, decode_unicode=True):

                if chunk:
                    f.write(chunk)

    return filepath


def zip_files(filename, members):
    filepath = '/tmp/%s' % filename.replace("/","_")
    with ZipFile(filepath, 'w') as receipts_zip:
        for _ in members:
            receipts_zip.write(_, basename(_))
    return filepath


def clean_trial_numbers(numbers):

    """

    Validates MIN list for trial broadcast.

    .. note::
        For trial broadcast, the maximum number of MINs allowed is 5

    :type numbers: list
    :param numbers: array of MINs for the trial broadcast

    :rtype: list
    :return: list of MINs after validation suceeds

    .. note:: Consult with Rence on this. Function possibly deprecated.

    """

    # WANT: Put to config.
    trial_max_count = 5

    if len(numbers) > trial_max_count:
        raise KastApiError({
            'error': ('You can only send up to %d phone numbers '
                'in trial broadcast.' % trial_max_count),
            'code': '013'}, 400)

    phonenumbers = []
    for _ in numbers:
        phone = phone_ph.parse(_)
        phonenumbers.append(phone.number[1:])

    return phonenumbers


def trial_send_confirm_email(ref_id, phonenumbers, sms_message, user_email):

    """

    Sends a confimation email to a list of recipients after a sucessful trial broadcast.

    :type ref_id: str
    :param ref_id: the trial broadcast's reference id  

    :type phonenumbers: int 
    :param phonenumbers: number of MINs that were broadcasted

    :type sms_message: str
    :param sms_message: the SMS message of the broadcast

    :type user_email: str
    :param user_email: recipient email(s) 

    .. warning::
         - Email subject is hardcoded
         - Email message is hardcoded with broadcast reference number iterated

    :rtype: None
    :return: Nothing

    """

    conf = settings.BROADCASTS

    subject = 'KAST Free Trial Broadcast Confirmation'
    message = ('You have successfully submitted your broadcast. '
        'Reference ID: %s') % ref_id
    html_message = render_to_string(
        'kast_bcast_app_templates/emails/trial_ok.html',
        {'created_at': arrow.utcnow().to('Asia/Manila').format(),
            'sender_id': conf['sender_id'],
            'phonenumbers': phonenumbers,
            'message': sms_message,
            'ref_id': ref_id})
    from_ = 'KAST'
    recipient_list = [user_email]
    send_mail(subject, message, from_, recipient_list,
        html_message=html_message)
    return


def live_user_send_confirm_email(
        ref_id, base_count, sms_message, user_email, sender_id, username, schedule):

    """

    Sends a confimation email to a list of recipients after a sucessful live broadcast.

    :type ref_id: str
    :param ref_id: the live broadcast's reference id  

    :type base_count: int 
    :param base_count: number of MINs that were broadcasted

    :type sms_message: str
    :param sms_message: the SMS message of the broadcast

    :type user_email: str
    :param user_email: recipient email(s) 

    :type sender_id: str
    :param sender_id: the sender ID used for the live broadcast 

    :type username: str
    :param username: username(email) of the campaign's owner 

    :type schedule: str
    :param schedule: schedule of the live broadcast     

    .. warning::
         - Email subject is hardcoded
         - Email message is hardcoded with iteration of both 'username' and 'ref_id'

    :rtype: None
    :return: Nothing

    """

    subject = 'KAST Broadcast Confirmation'
    message = ('Your account %s have successfully submitted sms broadcast. '
        'Reference ID: %s') % (user_email, ref_id)
    html_message = render_to_string(
        'kast_bcast_app_templates/emails/live_user_ok.html',
        {'created_at': arrow.utcnow().to('Asia/Manila').format(),
            'sender_id': sender_id,
            'username': username,
            'base_count': base_count,
            'message': sms_message,
            'ref_id': ref_id,
            'schedule': schedule,
            'campaign_type': 'sms_broadcast'})
    from_ = 'KAST'
    recipient_list = [user_email]
    send_mail(subject, message, from_, recipient_list,
        html_message=html_message)
    return


def live_admin_send_confirm_email(
        ref_id, base_count, sms_message, user_email, sender_id, username, schedule):

    """

    Sends a confimation email to the KAST admin after a sucessful live broadcast.

    :type ref_id: str
    :param ref_id: the live broadcast's reference id  

    :type base_count: int 
    :param base_count: number of MINs that were broadcasted

    :type sms_message: str
    :param sms_message: the SMS message of the broadcast

    :type user_email: str
    :param user_email: campaign owner's email address 

    :type sender_id: str
    :param sender_id: the sender ID used for the live broadcast 

    :type username: str
    :param username: username(email) of the campaign's owner 

    :type schedule: str
    :param schedule: schedule of the live broadcast     

    .. warning::
         - Email subject is hardcoded
         - Email message is hardcoded with iteration of both 'username' and 'ref_id'
         - Email recipient is hardcoded
         - 'username' and 'user_email' is redundant

    :rtype: None
    :return: Nothing

    """

    subject = 'KAST Broadcast by %s' % user_email
    message = ('Your account %s have successfully submitted sms broadcast. '
        'Reference ID: %s') % (user_email, ref_id)
    html_message = render_to_string(
        'kast_bcast_app_templates/emails/live_admin_ok.html',
        {'created_at': arrow.utcnow().to('Asia/Manila').format(),
            'sender_id': sender_id,
            'base_count': base_count,
            'username': username,
            'message': sms_message,
            'ref_id': ref_id,
            'schedule': schedule,
            'campaign_type': 'sms_broadcast'})
    from_ = 'KAST'
    recipient_list = ['kast_admin@risingtide.ph']
    send_mail(subject, message, from_, recipient_list,
        html_message=html_message)
    return


def get_report_urls(broadcast_id):

    """

    Gets the report URL of the broadcast.

    :type broadcast_id: int
    :param broadcast_id: the broadcast ID

    :rtype: str
    :return: report URL of the broadcast.

    :raise KastApiError 014: Broadcast has no reports.

    """

    conf = settings.BROADCASTS
    auth = HTTPBasicAuth(conf['user'], conf['pass'])

    broadcasts = requests.get('%s%s' % (conf['base_url'], broadcast_id), auth=auth)
    bcast = json.loads(broadcasts.text)

    report_urls = bcast['receipts']
    #filter(
    #    lambda x: x,
    #    map(lambda x: x.get('receipts'), jobs))

    if not report_urls:
        raise KastApiError({
            'error': 'Broadcast #%s have no reports.' % broadcast_id,
            'code': '014'}, 400)

    return report_urls


def update_broadcast(broadcast_id, update_args):

    """

    Update broadcast partial resources

    :type broadcast_id: int
    :param broadcast_id: the broadcast ID

    :type update_args: obj
    :param update_args: JSON object containing resources to be updated

    :rtype: obj
    :return: API hit response

    """
    
    conf = settings.BROADCASTS
    auth = HTTPBasicAuth(conf['user'], conf['pass'])
    res = requests.patch(conf['base_url']+broadcast_id, auth=auth, json=update_args)

    logger.info('Call to cancel broadcast returned: %s' % res.text)

    return res.json()


def has_record(transaction_id, numbers):
    existing = retrieve(transaction_id, False)
    if existing:
        return False
    else:
        record(transaction_id, numbers)
        return True


def retrieve(transaction_id, delete):
    name = _create_name(transaction_id)
    wdoc = util_redis.get(name)
    if delete:
        self._redis.delete(name)

    return wdoc


def record(transaction_id, wdoc):
    name = _create_name(transaction_id)
    result = util_redis.set(name, wdoc)
    # 3 days
    util_redis.expire(name, 259200)
    return result


def _create_name(transaction_id):
    return 'bcast:%s' % transaction_id

